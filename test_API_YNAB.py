import requests
from requests.structures import CaseInsensitiveDict
import json



headers = CaseInsensitiveDict()
headers["Accept"] = "application/json"
headers["Authorization"] = "Bearer 8f419ff2f9130d867dba60b46ed8bd33becadffcf7f89ce5c3cf73b5f2b468be"


def test_get_budget_details():
    global budget_ID
    url = "https://api.youneedabudget.com/v1/budgets"
    resp = requests.get(url, headers=headers)
    response_body = resp.json()
    assert response_body["data"]["budgets"][0]["name"] == "My Budget"
    budget_ID = response_body["data"]["budgets"][0]["id"]
    assert resp.status_code == 200


def test_Negative_get_budget_details():
    url = "https://api.youneedabudget.com/v1/budgets"
    resp = requests.get(url)
    response_body = resp.json()
    assert response_body["error"]["detail"] == "Unauthorized"
    assert resp.status_code == 401

def test_Create_bank_account():
    url = "https://api.youneedabudget.com/v1/budgets/"+budget_ID+"/accounts"
    body = {
  "account": {
    "name": "account",
    "type": "savings",
    "balance": "99999"
  }
    }
    resp = requests.post(url, headers=headers,json=body)
    response_body = resp.json()
    assert response_body["data"]["account"]["type"] == "savings"
    assert resp.status_code == 201

def test_Negative_caseCreate_bank_account():
    url = "https://api.youneedabudget.com/v1/budgets/"+budget_ID+"/accounts"
    body = {
  "account": {
    "name": "account",
    "type": "savings1234567",
    "balance": "99999"
  }
    }
    resp = requests.post(url, headers=headers,json=body)
    response_body = resp.json()
    assert response_body["error"]["id"] == "400"
    assert resp.status_code == 400


def test_get_payee_details():
    url = "https://api.youneedabudget.com/v1/budgets/"+budget_ID+"/payees"
    resp = requests.get(url, headers=headers)
    response_body = resp.json()
    assert response_body["data"]["payees"][0]["name"] == "Starting Balance"
    assert resp.status_code == 200
